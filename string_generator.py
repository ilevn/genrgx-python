from generator.visitors.generator_visitor import GeneratorVisitor
from parsing.default_tree_builder import DefaultTreeBuilder
from parsing.tree_builder import TreeBuilder


class StringGenerator:
    def __init__(self, expr, builder: TreeBuilder = None):
        self.builder = (builder or DefaultTreeBuilder)(expr)

    def generate(self):
        parsed_node = self.builder.parse()
        visitor = GeneratorVisitor()
        parsed_node.visit(visitor)
        return visitor.generated

    def as_infinite_sequence(self):
        for _ in iter(int, 1):
            yield self.generate()
