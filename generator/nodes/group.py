from generator.nodes.node import Node
from generator.visitors.node_visitor import NodeVisitor


class Group(Node):
    def __init__(self, index: int, node: Node):
        self.index = index
        self.node = node

    def visit(self, visitor: NodeVisitor):
        visitor.visit(self)

    def __str__(self):
        return f"Group[{self.index}]{{{self.node}}}"

    def __repr__(self):
        return self.__str__()
