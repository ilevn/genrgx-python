from generator.nodes.node import Node
from generator.visitors.node_visitor import NodeVisitor


class FinalSymbol(Node):
    def __init__(self, value: str):
        self.value = value

    def visit(self, visitor: NodeVisitor):
        visitor.visit(self)

    def __str__(self):
        return f"FinalSymbol{{{self.value=}}}"

    def __repr__(self):
        return self.__str__()
