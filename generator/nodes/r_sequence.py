from generator.nodes.node import Node
from generator.visitors.node_visitor import NodeVisitor


class RSequence(Node):
    def __init__(self, nodes):
        self.nodes = nodes

    def visit(self, visitor: NodeVisitor):
        visitor.visit(self)
