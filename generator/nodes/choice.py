from generator.nodes.node import Node
from generator.visitors.node_visitor import NodeVisitor


class Choice(Node):
    def __init__(self, nodes):
        self.nodes = nodes

    def visit(self, visitor: NodeVisitor):
        visitor.visit(self)

    def __str__(self):
        return f"Choice{{{self.nodes}}}"

    def __repr__(self):
        return self.__str__()
