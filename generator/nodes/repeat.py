from generator.nodes.node import Node
from generator.visitors.node_visitor import NodeVisitor


class Repeat(Node):
    def __init__(self, node: Node, _min: int, _max: int):
        self.node = node
        self.min = _min
        self.max = _max

    def visit(self, visitor: NodeVisitor):
        visitor.visit(self)

    @classmethod
    def minimum(cls, node, times: int):
        return cls(node, times, -1)

    def __str__(self):
        return f"Repeat[{self.min}..{self.max}]{{{self.node}}}"

    def __repr__(self):
        return self.__str__()
