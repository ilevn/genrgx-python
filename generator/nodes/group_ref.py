from generator.nodes.node import Node
from generator.visitors.node_visitor import NodeVisitor


class GroupRef(Node):
    def __init__(self, index: int):
        self.index = index

    def visit(self, visitor: NodeVisitor):
        visitor.visit(self)

    def __str__(self):
        return f"GroupRef[{self.index}]"

    def __repr__(self):
        return self.__str__()
