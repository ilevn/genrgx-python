from abc import ABC, abstractmethod

from generator.visitors.node_visitor import NodeVisitor


class Node(ABC):
    @abstractmethod
    def visit(self, visitor: NodeVisitor):
        return NotImplemented
