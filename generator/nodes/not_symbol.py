import re

from generator.nodes.node import Node
from generator.visitors.node_visitor import NodeVisitor


class NotSymbol(Node):
    def __init__(self, pattern):
        self.pattern = re.compile(pattern)

    def visit(self, visitor: NodeVisitor):
        visitor.visit(self)

    def __str__(self):
        return f"NotSymbol{{{self.pattern.pattern}}}"

    def __repr__(self):
        return str.__repr__()
