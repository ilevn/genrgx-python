import enum

from generator.nodes.node import Node
from generator.visitors.node_visitor import NodeVisitor

_ALL_SYMBOLS = {chr(i) for i in range(32, 127)}


class SType(enum.Enum):
    Positive = enum.auto()
    Negative = enum.auto()


class SymbolSet(Node):

    def __init__(self, symbols, _type: SType):
        self.symbols = symbols if _type == SType.Positive else _ALL_SYMBOLS.difference(symbols)
        self._type = _type

    def visit(self, visitor: NodeVisitor):
        visitor.visit(self)

    @classmethod
    def all(cls):
        return cls(_ALL_SYMBOLS, SType.Positive)
