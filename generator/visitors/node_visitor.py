from abc import ABC, abstractmethod


class NodeVisitor(ABC):
    @abstractmethod
    def visit(self, node):
        return NotImplemented
