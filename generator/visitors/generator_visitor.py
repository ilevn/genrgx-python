import os
import random

from generator.nodes import SymbolSet, Choice, FinalSymbol, Repeat, RSequence, NotSymbol, Group, GroupRef
from generator.visitors.node_visitor import NodeVisitor


class GeneratorVisitor(NodeVisitor):
    def __init__(self):
        self.char_pool = list(SymbolSet.all().symbols)
        self.result = []
        self.group_values = {}
        self._generated = None
        self.supported_nodes = {
            SymbolSet: self._visit_symbol_set,
            Choice: self._visit_choice,
            FinalSymbol: self._visit_final_symbol,
            Repeat: self._visit_repeat,
            RSequence: self._visit_r_sequence,
            NotSymbol: self._visit_not_symbol,
            Group: self._visit_group,
            GroupRef: self._visit_group_reference
        }

    @property
    def generated(self):
        if self._generated is None:
            self._generated = "".join(self.result)
        return self._generated

    def generate_random_string(self, string_base):
        capped_length = max(min(len(string_base), 10), 1)
        byte_array = os.urandom(capped_length)
        # Simple bit mask to generate random indices for our new string.
        indices = [e & 0xFF & (len(self.char_pool) - 1) for e in byte_array]
        return "".join(self.char_pool[x] for x in indices)

    def visit(self, node):
        type_ = type(node)
        try:
            visit_method = self.supported_nodes[type_]
        except KeyError:
            print(f"{type_.__name__} is not a supported Node type.")
        else:
            visit_method(node)

    def _visit_symbol_set(self, node):
        symbols = list(node.symbols)
        idx = random.randrange(len(symbols))
        self.result.append(symbols[idx])

    def _visit_choice(self, node):
        nodes = node.nodes
        idx = random.randrange(len(nodes))
        nodes[idx].visit(self)

    def _visit_final_symbol(self, node):
        self.result.append(node.value)

    def _visit_repeat(self, node):
        max_ = 100 if node.max == -1 else node.max
        repeat = node.min if node.min >= max_ else random.randint(node.min, max_)

        for _ in range(repeat):
            node.node.visit(self)

    def _visit_r_sequence(self, node):
        for node in node.nodes:
            node.visit(self)

    def _visit_not_symbol(self, node):
        pattern = node.pattern
        result = self.generate_random_string(pattern)
        while pattern.match(result) is not None:
            result = self.generate_random_string(result)

        self.result.append(result)

    def _visit_group(self, node):
        start = len(self.result)
        node.node.visit(self)
        self.group_values[node.index] = "".join(self.result[start:])

    def _visit_group_reference(self, node):
        self.result.append(self.group_values[node.index])
