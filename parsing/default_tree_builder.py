import enum
import string

from generator.nodes.choice import Choice
from generator.nodes.final_symbol import FinalSymbol
from generator.nodes.group import Group
from generator.nodes.group_ref import GroupRef
from generator.nodes.not_symbol import NotSymbol
from generator.nodes.r_sequence import RSequence
from generator.nodes.repeat import Repeat
from generator.nodes.symbol_set import SType, SymbolSet
from parsing.tree_builder import TreeBuilder
from parsing.view import StringView


class _GroupType(enum.Enum):
    POSITIVE_LOOKAHEAD = enum.auto()
    POSITIVE_LOOKBEHIND = enum.auto()
    NEGATIVE_LOOKAHEAD = enum.auto()
    NEGATIVE_LOOKBEHIND = enum.auto()
    NON_CAPTURE_GROUP = enum.auto()
    CAPTURE_GROUP = enum.auto()

    @property
    def is_negative(self):
        return self in (self.NEGATIVE_LOOKAHEAD, self.NEGATIVE_LOOKBEHIND)


def check(obj, predicate, error_msg):
    if not predicate(obj):
        raise RuntimeError(error_msg)


class DefaultTreeBuilder(TreeBuilder):
    def __init__(self, expr: str):
        self.view = StringView(expr)
        self.node = None
        self.next_group_idx = 1
        # Short-circuit some group type lookups.
        self.quick_group_lookup = {
            "?=": _GroupType.POSITIVE_LOOKAHEAD,
            "?:": _GroupType.NON_CAPTURE_GROUP,
            "?!": _GroupType.NEGATIVE_LOOKBEHIND
        }

    def process_group_type(self) -> _GroupType:
        token = self.view.read(2)
        try:
            return self.quick_group_lookup[token]
        except KeyError:
            # Check for look behinds.
            if token == "?<":
                next_char = self.view.get()
                if next_char == "!":
                    return _GroupType.NEGATIVE_LOOKBEHIND
                elif next_char != "=":
                    raise RuntimeError(f"Unknown symbol in pattern {self.view.context}")
            else:
                # Step back.
                self.view.index -= 2
                return _GroupType.CAPTURE_GROUP

    @staticmethod
    def to_final(res, nodes):
        if res:
            nodes.append(FinalSymbol("".join(res)))
            # Remove processed node range and return.
            del res[:len(res)]

    @staticmethod
    def handle_range(res, symbol_ranges, range_started):
        if not range_started:
            return False

        # Chars for our range, e.g A-Z.
        last_char = res[-1]
        first_char = res[-2]
        del res[:first_char]
        symbol_ranges.append([chr(c) for c in range(ord(first_char), ord(last_char) + 1)])

    def parse_group(self, current_group_type: _GroupType):
        capture_group_idx = None

        if current_group_type == _GroupType.CAPTURE_GROUP:
            capture_group_idx = self.next_group_idx
            self.next_group_idx += 1

        choices = []
        nodes = []
        result = []
        is_choice = False

        while not self.view.eof:
            token = self.view.get()
            if not token:
                break

            if token == "[":
                self.to_final(result, nodes)
                nodes.append(self.handle_character_variations())

            elif token == "(":
                self.to_final(result, nodes)
                group_type = self.process_group_type()

                if group_type.is_negative:
                    sub_pattern = self.view.read_until(")")
                    nodes.append(NotSymbol(sub_pattern))
                    # Move past closing ')'.
                    self.view.get()
                else:
                    nodes.append(self.parse_group(group_type))

            elif token == "|":
                # Special case for when '(|abc)' is used:
                if not result and not nodes:
                    choices.append(FinalSymbol(""))
                else:
                    self.to_final(result, nodes)
                    choices.append(self.sequence_or_not(nodes, choices, False, None))
                    nodes.clear()
                # Set marker for later.
                is_choice = True

            elif token == ")":
                self.to_final(result, nodes)
                # This might lead to unbalanced parens.
                if is_choice:
                    choices.append(self.sequence_or_not(nodes, choices, False, None))
                    nodes.clear()

                return self.sequence_or_not(nodes, choices, is_choice, capture_group_idx)

            elif token in "{*?+":
                if not result:
                    # Last node is going to get repeated.
                    repeat_node = nodes.pop(-1)
                else:
                    # Last char is going to get repeated.
                    to_repeat = result.pop(-1)
                    self.to_final(result, nodes)
                    repeat_node = FinalSymbol(to_repeat)

                nodes.append(self.handle_repeat(token, repeat_node))

            elif token == ".":
                self.to_final(result, nodes)
                nodes.append(SymbolSet.all())

            elif token == "\\":
                self.handle_escaped_char(result, nodes, True)
            else:
                result.append(token)

        self.to_final(result, nodes)
        return self.sequence_or_not(nodes, choices, is_choice, capture_group_idx)

    def handle_escaped_char(self, res, nodes, group_ref_allowed):
        token = self.view.get()

        if token.lower() == "d":
            # Digit modifier.
            self.to_final(res, nodes)
            digits = list(range(10))
            nodes.append(SymbolSet(digits, SType.Negative if token == "D" else SType.Positive))

        elif token.lower() == "s":
            # Whitespace
            self.to_final(res, nodes)
            nodes.append(SymbolSet(string.whitespace, SType.Negative if token == "S" else SType.Positive))

        elif token.lower() == "w":
            # Word
            digits_and_letters = string.digits + string.ascii_letters
            nodes.append(SymbolSet(digits_and_letters, SType.Negative if token == "W" else SType.Positive))

        elif token == "x":
            # Hex
            next_val = self.view.peek()
            if next_val == "{":
                self.view.get()
                hex_val = self.view.read_until("}")
                self.view.get()
            else:
                hex_val = self.view.read(2)

            res.append(int(hex_val, 16))

        elif token in [str(x) for x in range(1, 10)]:
            # Group reference.
            self.to_final(res, nodes)
            if group_ref_allowed:
                # Get group references in the form of '\x...'.
                digit_substring = self.view.take_while(lambda x: x.isdigit())
                nodes.append(GroupRef(int(digit_substring)))
            else:
                raise RuntimeError("Group reference is not expected here.")
        else:
            res.append(token)

    def handle_character_variations(self):
        symbol_set_type = SType.Positive

        if self.view.peek() == "^":
            symbol_set_type = SType.Negative
            self.view.get()

        result = []
        symbol_ranges = []
        range_started = False

        while not self.view.eof:
            token = self.view.get()
            if token == "]":
                self.handle_range(result, symbol_ranges, range_started)
                completed_set = symbol_ranges + (result or [])
                return SymbolSet(completed_set, symbol_set_type)

            elif token == "-":
                # Range token
                if self.view.peek() == "]" or self.view.peek(-2) == "[":
                    result.append(token)
                else:
                    range_started = True

            elif token == "\\":
                nodes = []
                self.handle_escaped_char(result, nodes, False)
                if range_started:
                    if nodes:
                        raise RuntimeError(
                            f"Cannot make range with a shorthand escape sequence before {self.view.context}")
                    range_started = self.handle_range(result, symbol_ranges, range_started)
                if nodes:
                    if len(nodes) > 1:
                        raise RuntimeError(
                            f"Multiple nodes found inside square brackets escape sequence before {self.view.context}")
                    else:
                        if not isinstance(nodes[0], SymbolSet):
                            error = "Unexpected node found inside square brackets" \
                                    f" escape sequence before {self.view.context}"
                            raise RuntimeError(error)

                        result.extend(nodes[0].symbols)
            else:
                result.append(token)
                range_started = self.handle_range(result, symbol_ranges, range_started)

    @staticmethod
    def sequence_or_not(nodes, choices, is_choice, capture_group_idx):
        if len(nodes) == 1:
            result_node = nodes[0]
        else:
            if is_choice:
                check(choices, lambda x: len(x) > 0, "Passed empty choice nodes.")
                result_node = Choice(choices)
            else:
                check(nodes, lambda x: len(x) > 0, "Passed empty nodes.")
                result_node = RSequence(nodes)

        return Group(capture_group_idx, result_node) if capture_group_idx is not None else result_node

    def handle_repeat(self, token, node):
        if token == "*":
            return Repeat.minimum(node, 0)
        if token == "?":
            return Repeat(node, 0, 1)
        if token == "+":
            return Repeat.minimum(node, 1)
        if token == "{":
            result = []
            repeat_min = -1
            while not self.view.eof:
                temp = self.view.get()
                if temp == ",":
                    repeat_min = int("".join(result))
                    result.clear()
                elif temp == "}":
                    repeater = int("".join(result))
                    if repeat_min == -1:
                        # No min bound.
                        return Repeat(node, repeater, repeater)
                    else:
                        if not result:
                            return Repeat.minimum(node, repeat_min)
                        return Repeat(node, repeat_min, repeater)
                elif temp == "\\":
                    temp = self.view.get()
                    result.append(temp)
                else:
                    result.append(temp)

            raise RuntimeError("Unbalanced '{' - missing '}'")

        raise RuntimeError(f"Unknown repetition character {token}")

    def _parse_tree(self):
        view = self.view
        if view.peek() == "^":
            view.get()
        if view.last == "$":
            view.end = view.end - 1

        node = self.parse_group(_GroupType.NON_CAPTURE_GROUP)
        if not view.eof:
            raise RuntimeError("Expression was not fully matched.")
        return node

    def parse(self):
        if self.node is None:
            self.node = self._parse_tree()
        return self.node
