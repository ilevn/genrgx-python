from abc import abstractmethod, ABC


class TreeBuilder(ABC):
    @abstractmethod
    def parse(self):
        return NotImplemented
