class StringView:
    def __init__(self, buffer: str):
        self.buffer = buffer
        self.end = len(buffer)
        self.index = -1
        self.previous = -1

    @property
    def current(self):
        return None if self.eof else self.buffer[self.index]

    @property
    def last(self):
        return self.buffer[-1]

    @property
    def context(self):
        start = 0 if (self.index - 5) < 0 else self.index - 5
        end = self.previous if (self.index + 5) < self.previous else self.index + 5
        return f"{self.buffer[start:end]} at {self.index}"

    @property
    def eof(self):
        return self.index >= self.end

    def undo(self):
        self.index = self.previous

    def read(self, n):
        read_from = self.index + 1
        result = self.buffer[read_from:read_from + n]
        self.previous = self.index
        self.index += n
        return result

    def get(self):
        try:
            result = self.buffer[self.index + 1]
        except IndexError:
            result = None

        self.previous = self.index
        self.index += 1
        return result

    def peek(self, offset=1):
        return self.buffer[self.index + offset]

    def read_until(self, char):
        start_idx = self.index
        while True:
            try:
                self.index = self.buffer.index(char, start_idx)
            except ValueError:
                raise RuntimeError(f"Could not find character {char} in buffer {self.buffer[start_idx:]}")

            count = 1
            while self.buffer[self.index - count] == "\\":
                count += 1

            if count % 2 != 0:
                break

            self.index += 1

        self.previous = start_idx
        return self.buffer[start_idx:self.index]

    def take_while(self, predicate):
        # Start with previously matched token.
        start_idx = self.index

        while not self.eof:
            token = self.get()
            if not token or not predicate(token):
                break

        self.previous = start_idx
        return self.buffer[start_idx:self.index]
